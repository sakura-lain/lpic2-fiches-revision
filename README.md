# LPIC-2 - Fiches de révision

**Des fiches pour réviser l'examen 201 de la certification LPIC-2.**

[Objectifs de l'examen 201](https://www.lpi.org/our-certifications/exam-201-objectives)

## Bibliographie :

- *Préparation à la certification LPIC-2*, livre de Sébastien Bobillier et Philippe Blanquet paru aux éditions Eni en 2014. [TP correspondants.](https://gitlab.com/sakura-lain/lpic2-tp)
- *Linux - Administration avancée : maintenance et exploitation de vos serveurs -3e édition* de Philippe Pinchon, paru aux éditions Eni en 2019.
- *LPIC-2 Study Guide* de Roderick W. Smith, paru aux éditions Wiley en 2011.

## Webographie :

- [LPIC-2 Exam Prep - unix.nl](http://lpic2.unix.nl/)
- Définitions et illustrations tirées en partie de Wikipédia.

### Gestion des ressources :

#### CPU et disques :

- [Understanding CPU Usage in Linux - OpsDash](https://www.opsdash.com/blog/cpu-usage-linux.html)
- [https://wiki.deimos.fr/Optimiser_les_performances_des_disques_dur_sur_Linux.html](https://wiki.deimos.fr/Optimiser_les_performances_des_disques_dur_sur_Linux.html)

#### Réseau :

- [ss - IT Connect](https://www.it-connect.fr/les-connexions-sockets-avec-la-commande-ss/)
- [netstat - Mon pense-bête](https://www.quennec.fr/trucs-astuces/syst%C3%A8mes/windows/toutes-versions/la-commande-netstat)

#### nice :

- [nice (et renice) : la priorité des processus sous Linux - IT Connect](https://www.it-connect.fr/nice-et-renice-la-priorite-des-processus-sous-linux/)
- [Gérer la priorité des processus sous Linux grâce à nice et ionice - Mind and Go](https://mind-and-go.com/blog/why-not-1/post/gerer-la-priorite-des-processus-sous-linux-grace-a-nice-et-ionice-100)

#### sar :

- [sar - IBM](https://www.ibm.com/support/knowledgecenter/en/ssw_aix_72/s_commands/sar.html)
- [sar - Oracle](https://docs.oracle.com/cd/E24843_01/html/E23087/spmonitor-8.html)
- [sar - Admin-Sys](http://www.admin-sys.org/sar)
- [Surveiller votre système avec SAR - Journal d'un admin Linux](https://journaldunadminlinux.fr/monitoring-surveiller-votre-systeme-avec-sar/)
- [sar etc. - Nidirondel](http://nidirondel.free.fr/llibre/sar_mes_infos.htm)
- [sadc - IBM](https://www.ibm.com/support/knowledgecenter/ssw_aix_72/s_commands/sadc.html)
- [sa1 - Oracle](https://docs.oracle.com/cd/E19683-01/817-1758/spconcepts-50595/index.html)

### Amorçage et bootloaders :

#### Grub et Grub 2 :

- [Grub 2 : paramétrage manuel - Ubuntu-fr](https://doc.ubuntu-fr.org/tutoriel/grub2_parametrage_manuel)
- [grub-pc - Ubuntu-fr](https://doc.ubuntu-fr.org/grub-pc)
- [Grub and mount errors - IT Knowledge Exchange](https://itknowledgeexchange.techtarget.com/linux-lotus-domino/grub-and-boot-errors/)
 
#### UEFI :

- [Shell UEFI - Papy Tux](https://papy-tux.legtux.org/doc1249/index.html)
- [La partition EFI ou ESP - Malekal](https://www.malekal.com/la-partition-efi-ou-esp/)
- [How to install Ubuntu using the EFI shell - AskUbuntu](https://askubuntu.com/questions/981267/how-to-install-ubuntu-using-the-efi-shell)
- [Le shell EFI - Linux Fr](https://linuxfr.org/news/uefi-%C3%A0-la-d%C3%A9couverte-du-nouveau-bios#toc_5)
- [UEFI shell - ArchLinux](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface#Obtaining_UEFI_Shell)
- [Grub/EFI examples - Archlinux](https://wiki.archlinux.org/index.php/GRUB/EFI_examples)
- [https://www.rodsbooks.com/efi-bootloaders/refind.html - Rod's Books](https://www.rodsbooks.com/efi-bootloaders/refind.html)
- [OpenSUSE : UEFI - OpenSUSE](https://fr.opensuse.org/openSUSE:UEFI)
- [Démarrage sécurisé UEFI Secure Boot - Red Hat](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/sec-uefi_secure_boot)
- [Secure Boot et UEFI - Linux Embedded](https://www.linuxembedded.fr/2017/10/secure-boot-et-uefi/)

#### SysLinux (IsoLinux, PXE, etc.) :

- [sysLinux - Wikipedia](https://fr.wikipedia.org/wiki/Syslinux)
- [ISOLINUX - SysLinux](https://wiki.syslinux.org/wiki/index.php?title=ISOLINUX)
- [Isohybrid - SysLinux](https://wiki.syslinux.org/wiki/index.php?title=Isohybrid)
- [PXE BIOS et UEFI avec pfSense - Adrien Furet](https://www.adrienfuret.fr/2015/08/18/pxe-bios-uefi-pfsense/)
- [Extlinux : Live-Usb sur une partition Ext2/Ext3/Ext4 - Ubuntu Fr](https://doc.ubuntu-fr.org/extlinux)

### Démarrage :
- [update-rc.d - manpage - Ubuntu.com](https://manpages.ubuntu.com/manpages/trusty/fr/man8/update-rc.d.8.html)
- [Lancer des scripts personnalisés au démarrage - Linux Tricks](https://www.linuxtricks.fr/wiki/lancer-des-scripts-personnalises-au-demarrage)
- [Créer et modifier des fichiers d'unité systemd - Red Hat](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-unit_files)

### Noyau :

#### Compilation et installation :

- [Compiler le noyau - Université de Toulouse](http://www.math.univ-toulouse.fr/~grundman/linux/HOWTO/Kernel-HOWTO-4.html)
- [Compilation du noyau - Linux France](http://www.linux-france.org/article/mininet/noyau/noyau-2.html)
- [initrd & initramfs - Linux.com](https://www.linux.com/tutorials/kernel-newbie-corner-initrd-and-initramfs-whats/)

#### DKMS :

- [Exploring Dynamic Kernel Module Support (DKMS) - Linux Journal](https://web.archive.org/web/20190813132009/https://www.linuxjournal.com/article/6896)
- [DKMS - Léa Linux](https://lea-linux.org/documentations/HOWTO_Dkms)

#### Modules :

- [Les modules Linux - Ubuntu-fr](https://doc.ubuntu-fr.org/tutoriel/tout_savoir_sur_les_modules_linux)
- [dracut - wikipedia](https://en.wikipedia.org/wiki/Dracut_(software))
- [Dracut troubleshooting - Famille Michon](https://famille-michon.fr/journalgeek/2012/08/29/dracut-and-past/)

#### Patch :

- [patch - Ubuntu-fr](https://doc.ubuntu-fr.org/patch)
- [diff et patch - Debian-fr](https://wiki.debian-fr.xyz/Utiliser_diff_et_patch)

### Systèmes de fichiers :

- [mkreiserfs - manpage - SysTutorials](https://www.systutorials.com/docs/linux/man/8-mkreiserfs/)
- [Snapshot et rollback sur votre système GNU/Linux - LinuxFr](https://linuxfr.org/news/btrfs-snapshot-et-rollback-sur-votre-systeme-gnu-linux)
- [How To Create Snapshots And Restore Your Linux System Using Btrfs - LinuxPitStop](http://linuxpitstop.com/snapshots-and-restore-linux-system-using-btrfs/)
- [La Sauvegarde de subvolumes BTRFS - Debian Facile](https://debian-facile.org/doc:systeme:btrfs-sauvegarde)
- [AutoFS - Ubuntu-fr](https://doc.ubuntu-fr.org/autofs)

### Gestion des disques :

#### hdparm et sdparm :

- [Utilisation de hdparm et sdparm - LeForumInfo](https://leforuminfo.com/obtenir-sn-hdd)
- [sdparm - sg.danny.cz](http://sg.danny.cz/sg/sdparm.html)
- [sdparm - eteindre et rallumer ses disques durs - LinuxFr](https://linuxfr.org/forums/astucesdivers/posts/sdparm-eteindre-et-rallumer-ses-disques-durs)

#### RAID :

- [Gérer un Raid logiciel sous Ubuntu - LeForumInfo](https://leforuminfo.com/gerer-raid-mdadm)
- [Récupération de données Raid 6 - Récupération de données Raid](https://www.recuperation-donnees-raid.com/raid6)
- [Disks, arrays and LUNs - Netapp](https://docs.netapp.com/ontap-9/index.jsp?topic=%2Fcom.netapp.doc.onc-sm-help-900%2FGUID-A259643B-6D4C-4B41-8999-9B56AAB8F34F.html)
- [Migrer votre distribution Debian en RAID - Développez.com](https://chrtophe.developpez.com/tutoriels/migrer-debian-raid/)
- [/proc/mdstat - Kernel.org](https://raid.wiki.kernel.org/index.php/Mdstat)

#### LVM :

- [LVM commands tutorial - Kernel Talks](https://kerneltalks.com/disk-management/lvm-commands-tutorial-pvchange-pvmove/)
- [pvchange et pvmove - Kernel Talks](https://kerneltalks.com/disk-management/lvm-commands-tutorial-pvchange-pvmove/)
- [Displaying Physical Volumes - Red Hat](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Cluster_Logical_Volume_Manager/physvol_display.html)
- [xfs_check and xfs_repair of LVM under Rescue Mode on Centos 7, OracleLinux 7 - Prasad Linux](https://prasadlinuxblog.wordpress.com/2017/12/26/xfs_check-and-xfs_repair-of-lvm-under-rescue-mode-on-centos-7-oraclelinux-7/)

#### iSCSI :

- [iSCSI connection command examples (Cheat Sheet)](https://www.thegeekdiary.com/iscsi-connection-command-examples-cheat-sheet/)
- [Initiateur iSCSI - Ubuntu-fr](https://guide.ubuntu-fr.org/server/iscsi-initiator.html)
- [Utiliser un SSD NVMe avec Linux, même sur un vieux serveur - Deltasight](https://www.deltasight.fr/ssd-nvme-linux-vieux-serveur/)
 
#### NVMe :

- [NVMe on Linux - Network World](https://www.networkworld.com/article/3397006/nvme-on-linux.html)
- [Using NVMe Command Line Tools to Check NVMe Flash Health - Percona](https://www.percona.com/blog/2017/02/09/using-nvme-command-line-tools-to-check-nvme-flash-health/)

#### Partitions :

- [Partitions types - Kernel.org](https://raid.wiki.kernel.org/index.php/Partition_Types)
- [Partitions types - plpd.org](https://www.tldp.org/HOWTO/Partition-Mass-Storage-Definitions-Naming-HOWTO/x190.html)

### Sauvegardes :

- [zcat- TecMint](https://www.tecmint.com/linux-zcat-command-examples/)
- [xz - IT Connect](https://www.it-connect.fr/compresser-et-decompresser-en-xz-et-tar-xz-sous-linux/)
- [gzip - Linux France](http://www.linux-france.org/article/memo/node129.html)
- [Backup and Recovery - Debian.org](https://wiki.debian.org/fr/BackupAndRecovery)

### Réseau :

#### Configuration :

- [Network Tools](https://network-tools.com/)
- [wireless-tools : configuration wifi - Debian Facile](https://debian-facile.org/doc:reseau:wireless-tools)
- [Configuration de réseau par les fichiers sysconfig - Red Hat](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/networking_guide/sec-network_configuration_using_sysconfig_files)
- [DHCP - La cahier de l'administrateur Debian](https://debian-handbook.info/browse/fr-FR/stable/sect.dhcp.html)

#### Routage :

- [ip route add - Cyberciti](https://www.cyberciti.biz/faq/ip-route-add-network-command-for-linux-explained/)
- [How to delete an IP route? - Unix stack Exchange](https://unix.stackexchange.com/questions/403511/how-to-delete-an-ip-route/403521)
- [mtr - Autour de Linux](https://www.leunen.com/linux/2011/06/mtr-un-outil-de-diagnostique-reseau/)
- [mtr - manpage - SysTutorials](https://www.systutorials.com/docs/linux/man/8-mtr/)

### Notifications aux utilisateurs

- [motd - Ubuntu-fr](https://doc.ubuntu-fr.org/motd)
- [shutdown - Linux France](http://www.linux-france.org/prj/edu/archinet/systeme/ch47s15.html)

## Autres ressources :

- Cours municipaux d'adultes de la Mairie de Paris sur Unix-Linux.
- Cours municipaux d'adultes de la Mairie de Paris sur les réseaux locaux.